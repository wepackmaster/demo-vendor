import React from "react";
import { Switch, Redirect, Route } from "react-router-dom";

import Home from "./components/home";
import Login from "./components/login";

const isUserLoggedIn = () => {};

function App() {
	return (
		<>
			<Switch>
				<Route
					exact
					path='/'
					render={(props) =>
						isUserLoggedIn() ? <Home {...props} /> : <Redirect to='/login' />
					}
				/>

				<Route exact path='/login'>
					<Login />
				</Route>
			</Switch>
		</>
	);
}

export default App;
