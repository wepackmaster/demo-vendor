import React, { useState } from "react";
import axios from "axios";

const handleSubmit = (event, username, password) => {
	event.preventDefault();
	let formData = new FormData();
	formData.append("username", username);
	formData.append("password", password);

	axios
		.post("/token", formData)
		.then(function (response) {
			console.log(response);
		})
		.catch(function (error) {
			console.log(error);
		});
};

const Login = () => {
	const [username, setUsername] = useState("");
	const [password, setPassword] = useState("");

	return (
		<form onSubmit={(event) => handleSubmit(event, username, password)}>
			<label>
				Username:
				<input
					type='text'
					name='name'
					onChange={(event) => setUsername(event.target.value)}
				/>
			</label>
			<label>
				Password:
				<input
					type='password'
					name='password'
					onChange={(event) => setPassword(event.target.value)}
				/>
			</label>
			<input type='submit' value='Submit' />
		</form>
	);
};

export default Login;
